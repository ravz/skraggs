<section class="content-header">
    <h1>
        Sliders
        <small>List of sliders</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Tables</a></li> -->
        <li class="active">Sliders</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-xs-12">
<div class="box">
<div class="box-header">
    <h3 class="box-title"></h3>
</div>
<!-- /.box-header -->
<div class="box-body">
<table id="example1" class="table table-bordered table-striped">
<thead>
<tr>
    <th>Id</th>
    <th>Image</th>
    <th>Title</th>
    <th>Description</th>
    <th>Action</th>
</tr>
</thead>
<tbody>
<?php
    if ($sliders) {

        foreach ($sliders as $t) {
            ?>
            <tr>
                <td><?php echo $t->id;?></td>
                <td><?php if($t->image != ""){ ?><img src="<?php echo base_url()?>uploads/sliders/<?php echo $t->image;?>" width="150"><?php } ?></td>
                <td><?php echo $t->title;?></td>
                <td><?php echo $t->description;?></td>
                <td>
                    <p>
                        <a href="<?php echo base_url()?>admin/sliders/edit/<?php echo $t->id;?>"><button type="button" class="btn bg-olive margin">Edit</button></a>
                        <a href="<?php echo base_url()?>admin/sliders/delete/<?php echo $t->id;?>"><button type="button" class="btn bg-red margin">Delete</button></a>
                    </p>
                </td>
            </tr>
<?php
        }
    }
?>
</tbody>
</table>

<p>
    <a href="<?php echo base_url();?>admin/sliders/add"><button type="button" class="btn bg-maroon btn-flat margin">Add Slider</button></a>
</p>

</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->