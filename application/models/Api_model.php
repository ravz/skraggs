<?php
	class Api_model extends CI_Model {
	
		function __construct() { 
			parent::__construct(); 			
		}

		//Generate a unique username using Database
		function generate_unique_username($string_name=" ", $rand_no = 200){

		    while(true){
		        $username_parts = array_filter(explode(" ", strtolower($string_name))); //explode and lowercase name
		        $username_parts = array_slice($username_parts, 0, 2); //return only first two arry part
		    
		        $part1 = (!empty($username_parts[0]))?substr($username_parts[0], 0,8):""; //cut first name to 8 letters
		        $part2 = (!empty($username_parts[1]))?substr($username_parts[1], 0,5):""; //cut second name to 5 letters
		        $part3 = ($rand_no)?rand(0, $rand_no):"";
		        
		        $username = $part1. str_shuffle($part2). $part3; //str_shuffle to randomly shuffle all characters 
		        
		        $username_exist_in_db = $this->username_exist_in_database($username); //check username in database
		        if(!$username_exist_in_db){
		            return $username;
		        }
		    }
		}


		function username_exist_in_database($username){
		        $result = $this->db->query("SELECT * FROM `users` WHERE `username` = '".$username."'");
		        return $result->num_rows();
		} 



		public function send_email($to,$subject,$content,$from = 'noreply@whichoneit.com'){
			$output = array();
			$headers  = "Organization: WhichOne \r\n";
			$headers .= "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			$headers .= "X-Priority: 1\r\n";
			$headers .= "X-Mailer: PHP". phpversion() ."\r\n"; 
			$headers .= "Reply-To: WhichOne <$from> \r\n";
			$headers .= "Return-Path: WhichOne <$from> \r\n";

			// More headers
			$headers .= "From: Which One <$from> \r\n";
			
			if(is_array($to)){
				if(sizeof($to) > 0){
					$to = implode(',',$to);
				}else{
					$output['error'] = "Please select at least one email address.";
					return $output;
				}
			}
			if(mail( $to, '[WhichOne] - '.$subject, $content, $headers)){
				 $output['success'] = "Mail sent.";
			}else{
				$output['error'] = "Unable to send email.";
			}
			return $output;
		}

		public function logout_user($user_id){
			$output = array();

			$data = array( 'access_token' => "");
			$this->db->where('id', $user_id);
			$this->db->update('users', $data);		 
			$output['response_code'] = 200;
			$output['status'] = "success";
			$output['status_message'] = "User logout successfull.";
			return $output;
		}

		/* APIs start here */
		public function register_media_user($data){
			$output = array();

			$this->db->where('email', $this->input->post('email'));
			$query = $this->db->get("users");
			if($query->num_rows() == 1){

				$tokenData = array();
                $tokenData['id'] = $query->row()->id;
                $tokenData['login_timestamp'] = time();

				$jwt_token = AUTHORIZATION::generateToken($tokenData);
				$output['response_code'] = 200;
				$output['status'] = 1;
				$output['status_message'] = "User exists already.";
				$output['token'] = $jwt_token;
				$output['user_details'] = array(
												'name'				=> $query->row()->first_name,
												'email'				=> $query->row()->email,
												'device_token'		=> $query->row()->device_token,
												'device_type'		=> $query->row()->device_type,
												'height_in_feet'	=> $query->row()->height_in_feet,
												'height_in_inches'	=> $query->row()->height_in_inches,
												'gender'			=> $query->row()->gender,
												'profession'		=> $query->row()->profession,
												'profile_picture'	=> $query->row()->profile_picture,
												'dob'				=> $query->row()->dob
											   );

				if($query->row()->profile_picture != ""){
					$output['user_details']['profile_picture'] = base_url('assets/uploads/profile').'/'.$query->row()->profile_picture;
				}

			}
			else{
				if (isset($data["email"]) && isset($data["device_type"]) && isset($data["gender"]) && isset($data["device_token"])  && $data["email"] != "" && $data["device_type"] != "" && $data["gender"] != "")
        		{
		            $gender = "";
		            if(($this->input->post('gender') == "Female") || ($this->input->post('gender') == "female")){
		                $gender = 1;
		            }
		            else if(($this->input->post('gender') == "Male") || ($this->input->post('gender') == "male")){
		                $gender = 2;
		            }
		            else{
		                $gender = 3;
		            }

					$data = array( 
	                    'first_name' => $this->input->post('first_name'),
                		'last_name' => $this->input->post('last_name'),
	                    'gender' => $gender,
	                    'device_token' => $this->input->post("device_token"),
	                    'device_type' => $this->input->post("device_type"),
	                );
					if($this->ion_auth->register($this->generate_unique_username($this->input->post('first_name').' '.$this->input->post('last_name'), 10), $data["password"], $data["email"], $additional_data) ){

						$insert_id = 0;
						if ($this->ion_auth->login($data["email"], $data["password"], $remember)){
							$insert_id = $this->session->userdata("user_id");
                		}

						if (is_numeric($insert_id)) {
	                        $tokenData = array();
	                		$tokenData['id'] = $insert_id;
	                		$tokenData['login_timestamp'] = time();

	                        $jwt_token = AUTHORIZATION::generateToken($tokenData);

	                        $this->db->where('email', $this->input->post('email'));
							$query = $this->db->get("users");

							$output['response_code'] = 200;
							$output['status'] = 1;
							$output['status_message'] = "User registered successfully.";
							$output['token'] = $jwt_token;
							$output['user_details'] = array(
														'name'				=> $query->row()->first_name,
														'email'				=> $query->row()->email,
														'device_token'		=> $query->row()->device_token,
														'device_type'		=> $query->row()->device_type,
														'height_in_feet'	=> $query->row()->height_in_feet,
														'height_in_inches'	=> $query->row()->height_in_inches,
														'gender'			=> $query->row()->gender,
														'profession'		=> $query->row()->profession,
														'profile_picture'	=> $query->row()->profile_picture,
														'dob'				=> $query->row()->dob
												   	  );
							if($query->row()->profile_picture != ""){
								$output['user_details']['profile_picture'] = base_url('assets/uploads/profile').'/'.$query->row()->profile_picture;
							}
			            }
					}else{
						$output['response_code'] = 200;
						$output['status'] = 0;
						$output['status_message'] = 'Unable to register user. Please try again!';
						$output['user_details'] = "No details of user.";
					}
				}else{
                	$output['response_code'] = 200;
					$output['status'] = 0;
					$output['status_message'] = 'Please send Email, Device Type, Device Token and Gender';
					$output['user_details'] = "No details of user.";
				}
			}
			return $output;
		}

		public function update_profile($user_id){
			$output = array();
			$picture = "";
			
			$this->db->where('id',$user_id);
			$query = $this->db->get('users');
			if($query->num_rows() == 1){

				$data = array(
					'dob' => date("Y-m-d", strtotime($this->input->post('dob'))),
					'height_in_feet' => $this->input->post('height_in_feet'),
					'height_in_inches' => $this->input->post('height_in_inches'),
					'profession' => $this->input->post('profession')
				);

				if(!empty($_FILES['profile_picture']['name'])){
					unlink('uploads/profile_pictures/'.$query->row()->profile_picture);

					$config['upload_path'] = 'uploads/profile_pictures/';
	                $config['allowed_types'] = 'jpg|jpeg|png|gif';
	                $config['max_width']            = 1024;
                	$config['max_height']           = 768;
               		$config['encrypt_name']         = true;
	                
	                //Load upload library and initialize configuration
	                $this->load->library('upload',$config);
	                $this->upload->initialize($config);
	                
	                if($this->upload->do_upload('profile_picture')){
	                    $uploadData = $this->upload->data();
	                    $data["profile_picture"] = $uploadData['file_name'];
	                }else{
	                    $output['status'] = 0;
						$output['status_message'] = $this->upload->display_errors();
						$output['response_code'] = 200;
						return $output;
	                }
	            }
				

	            $d = $this->input->post();

				if(isset($d['first_name']) && $d['first_name'] != ""){
					$data["first_name"] = $this->input->post('first_name');
				}

				if(isset($d['last_name']) && $d['last_name'] != ""){
					$data["last_name"] = $this->input->post('last_name');
				}

				if(isset($d['device_token']) && $d['device_token'] != ""){
					$data["device_token"] = $this->input->post('device_token');
				}

				if(isset($d['device_type']) && $d['device_type'] != ""){
					$data["device_type"] = $this->input->post('device_type');
				}

				$this->db->where('id', $user_id);
				$this->db->update('users',$data);
				$output['status'] = 1;
				$output['status_message'] = 'User profile updated successfully.';
				$output['response_code'] = 200;
			}else{
				$output['status'] = 0;
				$output['status_message'] = 'User record not found.';
				$output['response_code'] = 400;
			}
			return $output;
		}

		public function bodytype(){
			$output = array();
			$res = array();

			$gender = null;
            if(($this->input->get('gender') == "Female") || ($this->input->get('gender') == "female")){
                $gender = 1;
            }
            else if(($this->input->get('gender') == "Male") || ($this->input->get('gender') == "male")){
                $gender = 2;
            }
            else if(($this->input->get('gender') == "Both") || ($this->input->get('gender') == "both")){
                $gender = 3;
            }

			if(isset($gender)){

				// Bodytype
				$where = "";
				if($gender == 1 || $gender == 2){
					$where= "WHERE gender in ($gender,3) AND status=1";
				} 
				else{
					$where= "WHERE status=1";
				}
				$bodytype = $this->db->query("SELECT id AS bodytype_id, name AS bodytype_name FROM bodytype ".$where);

				// Result
				$output['status'] = 'success';
				$output['status_message'] = 'Details found.';
				$output['result'] = $bodytype->result();
				$output['response_code'] = 200;
			}else{
				$output['status'] = 'failure';
				$output['status_message'] = 'Gender field required.';
				$output['result'] = array();
				$output['response_code'] = 400;
			}
			return $output;
		}

		public function personality(){
			$output = array();

			$personality_url = base_url('assets/images/personality');
			$personality = $this->db->query("SELECT id AS personality_id, name AS personality_name, IF(female_image!='', CONCAT('".$personality_url."/',female_image), female_image) AS personality_female_image, IF(male_image!='',CONCAT('".$personality_url."/',male_image), male_image) AS personality_male_image FROM personality WHERE status=1");
			
			if($personality->num_rows() > 0){
				$output['status'] = 'success';
				$output['status_message'] = 'Details found.';
				$output['result'] = $personality->result();
				$output['response_code'] = 200;
			}
			else{
				$output['status'] = 'failure';
				$output['status_message'] = 'Personalities not found.';
				$output['result'] = array();
				$output['response_code'] = 200;
			}
			return $output;
		}

		public function lifestyle(){
			$output = array();

			$lifestyle_url = base_url('assets/images/lifestyle');
			$lifestyle = $this->db->query("SELECT id AS lifestyle_id, name AS lifestyle_name, IF(image!='',CONCAT('".$lifestyle_url."/',image), image) AS lifestyle_image FROM lifestyle WHERE status=1");
			$res['lifestyle'] = $lifestyle->result();
			
			if($lifestyle->num_rows() > 0){
				$output['status'] = 'success';
				$output['status_message'] = 'Details found.';
				$output['result'] = $lifestyle->result();
				$output['response_code'] = 200;
			}
			else{
				$output['status'] = 'failure';
				$output['status_message'] = 'Lifestyles not found.';
				$output['result'] = array();
				$output['response_code'] = 200;
			}
			return $output;
		}

		public function ethinicity(){
			$output = array();

			$ethinicity = $this->db->query("SELECT id AS ethinicity_id, name AS ethinicity_name FROM ethinicity WHERE status=1");
			if($ethinicity->num_rows() > 0){
				$output['status'] = 'success';
				$output['status_message'] = 'Details found.';
				$output['result'] = $ethinicity->result();
				$output['response_code'] = 200;
			}
			else{
				$output['status'] = 'failure';
				$output['status_message'] = 'Ethinicity not found.';
				$output['result'] = array();
				$output['response_code'] = 200;
			}
			return $output;
		}

		public function height_range(){
			$output = array();

			$height_range = $this->db->query("SELECT id AS height_range_id, value AS height_range FROM height_match_range WHERE status=1");
			
			if($height_range->num_rows() > 0){
				$output['status'] = 'success';
				$output['status_message'] = 'Details found.';
				$output['result'] = $height_range->result();
				$output['response_code'] = 200;
			}
			else{
				$output['status'] = 'success';
				$output['status_message'] = 'Height Ranges not found.';
				$output['result'] = array();
				$output['response_code'] = 200;
			}
			return $output;
		}

		public function attractiveness(){
			$output = array();

			$attractiveness = $this->db->query("SELECT id AS attractiveness_id, value AS attractiveness_value FROM attractiveness WHERE status=1");
			
			if($attractiveness->num_rows() > 0){
				$output['status'] = 'success';
				$output['status_message'] = 'Details found.';
				$output['result'] = $attractiveness->result();
				$output['response_code'] = 200;
			}
			else{
				$output['status'] = 'success';
				$output['status_message'] = 'Attractiveness not found.';
				$output['result'] = array();
				$output['response_code'] = 200;
			}
			return $output;
		}

		public function profession(){
			$output = array();

			$profession = $this->db->query("SELECT id AS profession_id, name AS profession_name FROM profession WHERE status=1");
			
			if($profession->num_rows() > 0){
				$output['status'] = 'success';
				$output['status_message'] = 'Details found.';
				$output['result'] = $profession->result();
				$output['response_code'] = 200;
			}
			else{
				$output['status'] = 'success';
				$output['status_message'] = 'Profession not found.';
				$output['result'] = array();
				$output['response_code'] = 200;
			}
			return $output;
		}

		public function update_user_attributes(){
			$output = array();
			
			$user_id = $this->input->post('user_id');
			$this->db->where('id',$user_id);
			$query = $this->db->get('users');
			if($query->num_rows() == 1){
				$data = array(
					'user_id' => $user_id,
					'bodytype_id' => $this->input->post('my_body_type_id'),
					'personality_id' => $this->input->post('my_personality_id'),
					'lifestyle_id' => $this->input->post('my_life_style_id'),
					'ethinicity_id' => $this->input->post('my_ethinicity_id')
				);
				if($this->db->insert('user_attributes', $data)){
					$output['status'] = 'success';
					$output['status_message'] = 'User attributes updated successfully.';
					$output['response_code'] = 200;
				}
				else{
					$output['status'] = 'error';
					$output['status_message'] = "User data updation error.";
					$output['response_code'] = 200;
				}
			}else{
				$output['status'] = 'failure';
				$output['status_message'] = 'User record not found.';
				$output['response_code'] = 400;
			}
			return $output;
		}

		public function update_users_match_attributes(){
			$output = array();
			
			$user_id = $this->input->post('user_id');
			$this->db->where('id',$user_id);
			$query = $this->db->get('users');

			$gender = null;
            if(($this->input->post('interested_gender') == "Female") || ($this->input->post('interested_gender') == "female")){
                $gender = 1;
            }
            else if(($this->input->post('interested_gender') == "Male") || ($this->input->post('interested_gender') == "male")){
                $gender = 2;
            }
            else if(($this->input->post('interested_gender') == "Both") || ($this->input->post('interested_gender') == "both")){
                $gender = 3;
            }

			if($query->num_rows() == 1){
				$data = array(
					'interested_in' => $gender,
					'height_match_id' => $this->input->post('ideal_match_height'),
					'interested_idealmatch_id' => $this->input->post('ideal_match_attractiveness'),
					'interested_bodytype_id' => $this->input->post('body_type'),
					'interested_profession_id' => $this->input->post('profession'),
					'interested_personality_id' => $this->input->post('personality'),
					'interested_lifestyle_id' => $this->input->post('life_style')
				);
				if($this->db->where('user_id', $user_id) && $this->db->update('user_attributes', $data)){
					$output['status'] = 'success';
					$output['status_message'] = "User's match updated successfully.";
					$output['response_code'] = 200;
				}
				else{
					$output['status'] = 'error';
					$output['status_message'] = "Data updation error.";
					$output['response_code'] = 200;
				}
			}else{
				$output['status'] = 'failure';
				$output['status_message'] = 'User record not found.';
				$output['response_code'] = 400;
			}
			return $output;
		}

		public function add_new_profile(){
			$output = array();
			
			$user_id = $this->input->post('user_id');
			$this->db->where('id',$user_id);
			$query = $this->db->get('users');

			$gender = null;
            if(($this->input->post('gender') == "Female") || ($this->input->post('gender') == "female")){
                $gender = 1;
            }
            else if(($this->input->post('gender') == "Male") || ($this->input->post('gender') == "male")){
                $gender = 2;
            }
            else if(($this->input->post('gender') == "Both") || ($this->input->post('gender') == "both")){
                $gender = 3;
            }

			if($query->num_rows() == 1){
				$data = array(
					'name' => $this->input->post('name'),
					'dob' => date("Y-m-d", strtotime($this->input->post('dob'))),
					'height_in_feet' => $this->input->post('height_in_feet'),
					'height_in_inches' => $this->input->post('height_in_inches'),
					'gender' => $gender,
					'profession' => $this->input->post('profession'),
					'status' => 1,
					'created_by' => $user_id,
					'created_on' => date('Y-m-d H:i:s'),
				);
				if(isset($_FILES['profile_picture']) && $_FILES['profile_picture']['error'] == 0){
					$config['upload_path'] = 'assets/uploads/profile';
					$config['allowed_types'] = 'jpg|jpeg|png|gif';
					
					//Load upload library and initialize configuration
					$this->load->library('upload',$config);
					$this->upload->initialize($config);
					
					if($this->upload->do_upload('profile_picture')){
						$uploadData = $this->upload->data();
						$data['profile_picture'] = $uploadData['file_name'];
					}else{
						$output['status'] = 0;
						$output['status_message'] = $this->upload->display_errors();
						$output['response_code'] = 200;
						$output['result'] = array();
						return $output;
					}
				}
				if($this->db->insert('user_profiles',$data)){
					$output['status'] = 'success';
					$output['status_message'] = 'Profile added successfully.';
					$output['response_code'] = 200;
					$output['result'] = array();
				}else{
					$output['status'] = 'failure';
					$output['status_message'] = 'Profile not added.';
					$output['response_code'] = 400;
					$output['result'] = array();
				}
			}else{
				$output['status'] = 'failure';
				$output['status_message'] = 'User record not found.';
				$output['response_code'] = 400;
				$output['result'] = array();
			}
			return $output;
		}

		public function get_dating_profiles(){
			$output = array();
			
			$user_id = $this->input->get('user_id');
			$this->db->where('id',$user_id);
			$query = $this->db->get('users');

			if($query->num_rows() == 1){

				$image_url = base_url('assets/uploads/profile');
				$query = $this->db->query("
					SELECT id, name, DATE_FORMAT(dob, '%d-%m-%Y') AS dob, height_in_feet, height_in_inches, IF(gender=1 ,'female', 'male') AS gender, profession, IF(profile_picture!='',CONCAT('".$image_url."/',profile_picture), profile_picture) AS profile_picture
					FROM user_profiles
					WHERE created_by=$user_id
				");
				if($query->num_rows() > 0){
					$output['status'] = 'success';
					$output['status_message'] = 'Profiles list.';
					$output['result'] = $query->result();
					$output['response_code'] = 200;
				}else{
					$output['status'] = 'failure';
					$output['status_message'] = 'Profiles not found.';
					$output['result'] = array();
					$output['response_code'] = 400;
				}
			}else{
				$output['status'] = 'failure';
				$output['status_message'] = 'User record not found.';
				$output['result'] = array();
				$output['response_code'] = 400;
			}
			return $output;
		}

		public function update_other_profile(){
			$output = array();
			
			$user_id = $this->input->post('user_id');
			$id = $this->input->post('other_user_id');
			$this->db->where('id',$user_id);
			$query = $this->db->get('users');

			$gender = null;
            if(($this->input->post('gender') == "Female") || ($this->input->post('gender') == "female")){
                $gender = 1;
            }
            else if(($this->input->post('gender') == "Male") || ($this->input->post('gender') == "male")){
                $gender = 2;
            }
            else if(($this->input->post('gender') == "Both") || ($this->input->post('gender') == "both")){
                $gender = 3;
            }

			if($query->num_rows() == 1){
				$data = array(
					'name' => $this->input->post('name'),
					'dob' => date("Y-m-d", strtotime($this->input->post('dob'))),
					'height_in_feet' => $this->input->post('height_in_feet'),
					'height_in_inches' => $this->input->post('height_in_inches'),
					'gender' => $gender,
					'profession' => $this->input->post('profession')
				);
				if(isset($_FILES['profile_picture']) && $_FILES['profile_picture']['error'] == 0){
					$config['upload_path'] = 'assets/uploads/profile';
					$config['allowed_types'] = 'jpg|jpeg|png|gif';
					
					//Load upload library and initialize configuration
					$this->load->library('upload',$config);
					$this->upload->initialize($config);
					
					if($this->upload->do_upload('profile_picture')){
						$uploadData = $this->upload->data();
						$data['profile_picture'] = $uploadData['file_name'];
					}else{
						$output['status'] = 'failed';
						$output['status_message'] = $this->upload->display_errors();
						$output['response_code'] = 400;
						return $output;
					}
				}
				if(($this->db->where('id',$id)) && ($this->db->update('user_profiles',$data))){
					$output['status'] = 'success';
					$output['status_message'] = 'Profile updated successfully.';
					$output['response_code'] = 200;
				}else{
					$output['status'] = 'failure';
					$output['status_message'] = 'Profile not updated.';
					$output['response_code'] = 400;
				}
			}else{
				$output['status'] = 'failure';
				$output['status_message'] = 'User record not found.';
				$output['response_code'] = 400;
			}
			return $output;
		}

		public function user_profile_details(){
			$output = array();
			$image_url = base_url('assets/uploads/profile');

			$user_id = $this->input->get('user_id');
			$this->db->where('id',$user_id);
			$query = $this->db->query("
				SELECT id, username, email, DATE_FORMAT(dob, '%d-%m-%Y'), height_in_feet, height_in_inches, IF(gender=1 ,'female', 'male') AS gender, profession, IF(profile_picture!='',CONCAT('".$image_url."/',profile_picture), profile_picture) AS profile_picture FROM users WHERE id=$user_id
				");
			if($query->num_rows() > 0){
				$output['status'] = 'success';
				$output['status_message'] = 'User found.';
				$output['result'] = $query->result_array();
				$output['response_code'] = 200;
			}
			else{
				$output['status'] = 'success';
				$output['status_message'] = 'User not found.';
				$output['result'] = array();
				$output['response_code'] = 200;
			}
			return $output;
		}


		public function sliders(){
			$output = array();
			$image_url = base_url('assets/uploads/profile');

			$user_id = $this->input->get('user_id');
			$this->db->where('id',$user_id);
			$query = $this->db->query("
					SELECT * FROM `sliders` WHERE `status` = 1
				");
			$res = $query->result_array();
			foreach ($res as $key => $v) {
				$res[$key]["image"] = base_url()."uploads/sliders/".$v["image"];
			}

			if($query->num_rows() > 0){
				$output['status'] = 'success';
				$output['status_message'] = 'Sliders found.';
				$output['result'] = $res;
				$output['response_code'] = 200;
			}
			else{
				$output['status'] = 'success';
				$output['status_message'] = 'Sliders not found.';
				$output['result'] = array();
				$output['response_code'] = 200;
			}
			return $output;
		}


		public function get_all_attributes($user_id){
			$res = array();

			// Get body type
			$this->db->where('status', 1);
			$this->db->select('id, name, gender');
			$q = $this->db->get('bodytype');
			foreach($q->result_array() as $k => $v){
				$res["bodytype"][$k] = $v;
			}

			// Get ethinicity
			$this->db->where('status', 1);
			$this->db->select('id, name');
			$q = $this->db->get('ethinicity');
			foreach($q->result_array() as $k => $v){
				$res["ethinicity"][$k] = $v;
			}

			// Interested In
			$res["interested_in"][] = array("id" => 1, "gender" => "Female");
			$res["interested_in"][] = array("id" => 2, "gender" => "Male");
			$res["interested_in"][] = array("id" => 3, "gender" => "Both");

			// My Personality
			$this->db->where('status', 1);
			$this->db->select('id, name, female_image, male_image');
			$q = $this->db->get('personality');
			foreach($q->result_array() as $k => $v){
				$v["female_image"] 	=	base_url()."assets/images/personality/".$v["female_image"];
				$v["male_image"]	=	base_url()."assets/images/personality/".$v["male_image"];
				$res["personality"][$k] = $v;
			}

			// My Lifestyle
			$this->db->where('status', 1);
			$this->db->select('id, name, image');
			$q = $this->db->get('lifestyle');
			foreach($q->result_array() as $k => $v){
				$v["image"] 			=	base_url()."assets/images/lifestyle/".$v["image"];
				$res["lifestyle"][$k] 	= 	$v;
			}

			// Attractiveness
			$this->db->where('status', 1);
			$this->db->select('id, value');
			$q = $this->db->get('height_match_range');
			foreach($q->result_array() as $k => $v){
				$res["height_match_range"][$k] 	= 	$v;
			}

			// Height Match Range
			$this->db->where('status', 1);
			$this->db->select('id, value');
			$q = $this->db->get('profession');
			foreach($q->result_array() as $k => $v){
				$res["profession"][$k] 	= 	$v;
			}

			// Age Range
			$this->db->where('status', 1);
			$this->db->select('id, value');
			$q = $this->db->get('age_range');
			foreach($q->result_array() as $k => $v){
				$res["age_range"][$k] 	= 	$v;
			}


			/*foreach ($res as $key => $v) {
				$res[$key]["image"] = base_url()."uploads/sliders/".$v["image"];
			}*/

			if($q->num_rows() > 0){
				$output['status'] = 1;
				$output['status_message'] = 'Attributes found.';
				$output['result'] = $res;
				$output['response_code'] = 200;
			}
			else{
				$output['status'] = 0;
				$output['status_message'] = 'Attributes not found.';
				$output['result'] = array();
				$output['response_code'] = 200;
			}

			return $output;
		}


		public function remove_tags($i){
			return strip_tags($i);
		}

		public function update_all_attributes($user_id, $data){
			$res = array();

			$data = array_map(array($this, 'remove_tags'), $data);

			$this->db->where('user_id', $user_id);
			$q = $this->db->get('user_attributes');

			if($q->num_rows() > 0){
				$this->db->where('user_id', $user_id);
				$this->db->update('user_attributes', $data);
			}else{
				$data["user_id"] = $user_id;
				$this->db->insert('user_attributes', $data);
			}

			if($user_id > 0){
				$output['status'] = 1;
				$output['status_message'] = 'Attributes updated.';
				$output['result'] = $res;
				$output['response_code'] = 200;
			}else{
				$output['status'] = 0;
				$output['status_message'] = 'Error while updating.';
				$output['result'] = array();
				$output['response_code'] = 200;
			}

			return $output;
		}



		public function get_user_info($user_id){
			$this->db->where('id', $user_id);
			$query = $this->db->get("users");

			if($query->num_rows() == 1){			
				$output['response_code'] = 200;
				$output['status'] = 1;
				$output['status_message'] = "User exists already.";
				$output['result'] = array(
												'name'				=> $query->row()->first_name,
												'email'				=> $query->row()->email,
												'device_token'		=> $query->row()->device_token,
												'device_type'		=> $query->row()->device_type,
												'height_in_feet'	=> $query->row()->height_in_feet,
												'height_in_inches'	=> $query->row()->height_in_inches,
												'gender'			=> $query->row()->gender,
												'profession'		=> $query->row()->profession,
												'dob'				=> $query->row()->dob
											   );
				if($query->row()->profile_picture != ""){
					$output['user_details']['profile_picture'] = base_url('assets/uploads/profile').'/'.$query->row()->profile_picture;
				}
			}else{
				$output['status'] = 0;
				$output['status_message'] = 'User record not found.';
				$output['response_code'] = 200;
				$output['result'] = array();
			}
			return $output;
		}


		public function get_all_user_attributes($user_id){
			$this->db->where('user_id', $user_id);
			$query = $this->db->get("user_attributes")->result_array();

			if(count($query) == 1){			
				$output['response_code'] = 200;
				$output['status'] = 1;
				$output['status_message'] = "User exists already.";
				$output['result'] = $query;
			}else{
				$output['status'] = 0;
				$output['status_message'] = 'User record not found.';
				$output['response_code'] = 200;
				$output['result'] = array();
			}
			return $output;
		}



		public function update_about_user($user_id, $data){
			$res = array();

			$data = array_map(array($this, 'remove_tags'), $data);

			$ud = array(
							"dob" => date("Y-m-d", strtotime($data["dob"])),
							"height_in_feet" => $data["height_in_feet"],
							"height_in_inches" => $data["height_in_inches"],
							"gender" => $data["gender"],
							"profession" => $data["profession"]
				  	   );
			$this->db->where('id', $user_id);
			$this->db->update('users', $ud);

			$ua = array(
							"bodytype" => $data["bodytype"],
							"ethinicity" => $data["ethinicity"],
							"interested_in" => $data["interested_in"],
							"personality" => $data["personality"],
							"lifestyle" => $data["lifestyle"]
				  	   );
			$this->db->where('user_id', $user_id);
			$this->db->update('user_attributes', $ua);

			if($user_id > 0){
				$output['status'] = 1;
				$output['status_message'] = 'Attributes updated.';
				$output['result'] = $res;
				$output['response_code'] = 200;
			}else{
				$output['status'] = 0;
				$output['status_message'] = 'Error while updating.';
				$output['result'] = array();
				$output['response_code'] = 200;
			}

			return $output;
		}


		public function update_ideal_match($user_id, $data){
			$res = array();

			$data = array_map(array($this, 'remove_tags'), $data);

			$this->db->where('user_id', $user_id);
			$this->db->update('user_attributes', $data);

			if($user_id > 0){
				$output['status'] = 1;
				$output['status_message'] = 'Attributes updated.';
				$output['result'] = $res;
				$output['response_code'] = 200;
			}else{
				$output['status'] = 0;
				$output['status_message'] = 'Error while updating.';
				$output['result'] = array();
				$output['response_code'] = 200;
			}

			return $output;
		}


		public function get_app_list($user_id){
			$this->db->where("status",1);
			$res = $this->db->get("app_list");
			if($res->num_rows() > 0){
				$output['status'] = 1;
				$output['status_message'] = 'Apps retreived.';
				$output['result'] = $res->result_array();
				$output['response_code'] = 200;
			}else{
				$output['status'] = 0;
				$output['status_message'] = 'Error while fetching.';
				$output['result'] = array();
				$output['response_code'] = 200;
			}
			return $output;
		}


		public function add_new_match($user_id, $data){
			$output = array();
			$picture = "";
			
			$this->db->where('id',$user_id);
			$query = $this->db->get('users');
			if($query->num_rows() == 1){
				$data["user_id"] = $user_id;
				if(isset($_FILES['profile_picture']['name']) && !empty($_FILES['profile_picture']['name'])){
					$config['upload_path'] = 'uploads/match_pictures/';
	                $config['allowed_types'] = 'jpg|jpeg|png|gif';
	                $config['max_width']            = 2000;
                	$config['max_height']           = 2000;
               		$config['encrypt_name']         = true;
	                
	                //Load upload library and initialize configuration
	                $this->load->library('upload',$config);
	                $this->upload->initialize($config);
	                
	                if($this->upload->do_upload('profile_picture')){
	                    $uploadData = $this->upload->data();
	                    $data["profile_picture"] = $uploadData['file_name'];
	                }else{
	                    $output['status'] = 0;
						$output['status_message'] = strip_tags($this->upload->display_errors());
						$output['response_code'] = 200;
						$output['result'] = array();
						return $output;
	                }
	            }
				$this->db->insert('user_profiles', $data);

				$output['status'] = 1;
				$output['status_message'] = 'User profile created successfully.';
				$output['response_code'] = 200;
				$output['result'] = array( "id" => $this->db->insert_id() );
			}else{
				$output['status'] = 0;
				$output['status_message'] = 'User record not found.';
				$output['response_code'] = 200;
			}
			return $output;
		}


		public function update_match($user_id, $update_id, $data){
			$output = array();
			$picture = "";

			$this->db->where('id',$update_id);
			$query = $this->db->get('user_profiles');

			if($query->num_rows() > 0){
				if(isset($_FILES['profile_picture']['name']) && !empty($_FILES['profile_picture']['name'])){
					unlink('uploads/match_pictures/'.$query->row()->profile_picture);

					$config['upload_path'] = 'uploads/match_pictures/';
	                $config['allowed_types'] = 'jpg|jpeg|png|gif';
	                $config['max_width']            = 2000;
                	$config['max_height']           = 2000;
               		$config['encrypt_name']         = true;
	                
	                //Load upload library and initialize configuration
	                $this->load->library('upload',$config);
	                $this->upload->initialize($config);
	                
	                if($this->upload->do_upload('profile_picture')){
	                    $uploadData = $this->upload->data();
	                    $data["profile_picture"] = $uploadData['file_name'];
	                }else{
	                    $output['status'] = 0;
						$output['status_message'] = strip_tags($this->upload->display_errors());
						$output['response_code'] = 200;
						$output['result'] = array();
						return $output;
	                }
	            }

	            $this->db->where("id", $update_id);
				$this->db->update('user_profiles', $data);

				$output['status'] = 1;
				$output['status_message'] = 'User profile created successfully.';
				$output['response_code'] = 200;
				$output['result'] = array( "id" => $this->db->insert_id() );
			}else{
				$output['status'] = 0;
				$output['status_message'] = 'User profile not found.';
				$output['response_code'] = 200;
				$output['result'] = array();
			}
			return $output;
		}


		public function get_matches($user_id){
			$this->db->where("status",1);
			$this->db->select('id, full_name, profession, meeting_place, age_range, height_range, attractiveness, bodytype, personality, lifestyle, IF(profile_picture = \'\', profile_picture, CONCAT(\''.base_url('uploads/match_pictures/').'\',profile_picture)) as profile_picture');
			$res = $this->db->get("user_profiles");
			if($res->num_rows() > 0){
				$output['status'] = 1;
				$output['status_message'] = 'Profile retreived.';
				$output['result'] = $res->result_array();
				$output['response_code'] = 200;
			}else{
				$output['status'] = 0;
				$output['status_message'] = 'Error while fetching profile.';
				$output['result'] = array();
				$output['response_code'] = 200;
			}
			return $output;
		}


		public function archive_match($user_id, $id){
			$this->db->where("id", $id);
			$res = $this->db->update("user_profiles", array("status" => 0));

			if($res){
				$output['status'] = 1;
				$output['status_message'] = 'Profile has been archived.';
				$output['result'] = array("id" => $id);
				$output['response_code'] = 200;
			}else{
				$output['status'] = 0;
				$output['status_message'] = 'Error while archiving the profile.';
				$output['result'] = array();
				$output['response_code'] = 200;
			}
			return $output;
		}


	}
?>