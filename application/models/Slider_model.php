<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slider_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function list_sliders(){
    	return $this->db->query("SELECT * FROM `sliders`")->result();
    }

    function add($data, $file){
    	$data = array(
		    'image'   		=> $file,
		    'title'   		=> $data["db_title"],
		    'description'   => $data["db_description"],
		    'position' 		=> $data["db_sort_order"],
		    'status' 		=> $data["status"]
		);

		$this->db->insert('sliders', $data);
		return $this->db->insert_id();
    }

    function edit($data, $file){

    	$existingFile = $this->db->query("SELECT `image` FROM `sliders` WHERE `id` = ".$data["slider_id"])->row();
    	if($file == "" || ($file != "" && $file != $existingFile->image)){    		
    		unlink("./uploads/sliders/".$existingFile->image);
    	}

    	$d = array(
		    'image'   		=> $file,
		    'title'   		=> $data["db_title"],
		    'description'   => $data["db_description"],
		    'position' 		=> $data["db_sort_order"],
		    'status' 		=> $data["status"]
		);
    	$this->db->where('id', $data["slider_id"]);
		$this->db->update('sliders', $d);
		return $data["slider_id"];
    }

    function get_slider_info($id){
    	return $this->db->query("SELECT * FROM `sliders` WHERE `id` = ".$id)->row();
    }

}