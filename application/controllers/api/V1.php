<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class V1 extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key

        $this->load->library(array('ion_auth','form_validation'));
        $this->load->helper(array('url','language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->load->model('Api_model');
    }
    
    public function login_post()
    {
        $data = $this->post();

        if (isset($data["email"]) && isset($data["password"]) && $data["email"] != "" && $data["password"] != "")
        {
            // check to see if the user is logging in
            // check for "remember me"
            $remember = false;
            if ($this->ion_auth->login($data["email"], $data["password"], $remember))
            {
                $tokenData = array();
                $tokenData['id'] = $this->session->userdata("user_id"); //TODO: Replace with data for token
                $tokenData['login_timestamp'] = time();
                $tokenData['group'] = $this->ion_auth->group_access_details($this->session->userdata("user_id"));
                $output['token'] = AUTHORIZATION::generateToken($tokenData);
                $output["group"] = $this->ion_auth->group_access_details($this->session->userdata("user_id"));
                $output["status"] = 2;
            }
            else
            {
                $output["error"] = "Invalid Email/password.";
                $output["status"] = 1;
            }
        }
        else
        {
            $output["error"] = "Please enter Email/Password";
            $output["status"] = 0;
        }

        $this->set_response($output, REST_Controller::HTTP_OK);
    }

    public function login_social_post()
    {
        $data = $this->post();

        if (isset($data["email"]) && $data["email"] != "")
        {
            // check to see if the user is logging in
            // check for "remember me"
            $remember = false;
            $r = $this->db->query("SELECT * FROM `users` WHERE `username` = '".$data["email"]."'");
            if ($r->num_rows() > 0)
            {
                $tokenData = array();
                $tokenData['id'] = $r->row()->id; //TODO: Replace with data for token
                $tokenData['login_timestamp'] = time();
                $tokenData['group'] = $this->ion_auth->group_access_details($r->row()->id);
                $output['token'] = AUTHORIZATION::generateToken($tokenData);
                $output["group"] = $this->ion_auth->group_access_details($r->row()->id);
                $output["status"] = 2;
            }
            else
            {
                $output["error"] = "Invalid Email.";
                $output["status"] = 1;
            }
        }
        else
        {
            $output["error"] = "Please send Email.";
            $output["status"] = 0;
        }

        $this->set_response($output, REST_Controller::HTTP_OK);
    }

    public function register_post()
    {
        $data = $this->post();

        if(isset($data["via"]) && $data["via"] == 2){
            $data["password"] = md5(time()."".$data["email"]); // Random Password
        }

        if (isset($data["email"]) && isset($data["password"]) && isset($data["register_type"]) && isset($data["via"])  && $data["email"] != "" && $data["password"] != "" && $data["register_type"] != "" && $data["via"] != "")
        {
            $name = explode(" ",$data['name']);
            $gender == "";
            if(($data['gender'] == "Female") || ($data['gender'] == "female")){
                $gender = 1;
            }
            else if(($data['gender'] == "Male") || ($data['gender'] == "male")){
                $gender = 2;
            }
            else if(($data['gender'] == "Both") || ($data['gender'] == "both")){
                $gender = 3;
            }

            $additional_data = array(
                'first_name' => $name[0],
                'last_name' => $name[1],
                'dob' => date("Y-m-d", strtotime($data['dob'])),
                'gender' => $gender,
                'device_type' => $data['device_type'],
                'device_token' => $data['device_token']
            );

            if ($this->ion_auth->register($data["email"], $data["password"], $data["email"], $additional_data, array(($data["register_type"])))){
                $remember = false;
                if ($this->ion_auth->login($data["email"], $data["password"], $remember))
                {
                    $time = time();
                    $this->ion_auth_model->update_last_login($this->session->userdata("user_id"), $time);

                    $tokenData = array();
                    $tokenData['id'] = $this->session->userdata("user_id"); //TODO: Replace with data for token
                    $tokenData['login_timestamp'] = $time;
                    $tokenData['group'] = $this->ion_auth->group_access_details($this->session->userdata("user_id"));
                    $output['token'] = AUTHORIZATION::generateToken($tokenData);
                    $output["group"] = $this->ion_auth->group_access_details($this->session->userdata("user_id"));
                    $output["status"] = 2;
                }
                else
                {
                    $output["error"] = "Invalid Email/password.";
                    $output["status"] = 1;
                }
            }else{
                $message = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
                $output["error"] = strip_tags($message);
                $output["status"] = 1;
            }
        }
        else
        {
            $output["error"]    = "Please enter all fields.";
            $output["status"]   = 0;
        }

        $this->set_response($output, REST_Controller::HTTP_OK);
    }

    // forgot password
    public function forgot_password_post()
    {

        $identity_column = $this->config->item('identity','ion_auth');

        $data = $this->post();
        $identity = $this->ion_auth->where($identity_column, $data["email"])->users()->row();

        if(empty($identity)) {
            $output["error"]    = "Please enter all fields.";
            $output["status"]   = 0;
        }else{
            // run the forgotten password method to email an activation code to the user
            $forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

            if ($forgotten)
            {
                $output["message"] = "Temporary password has been sent successfully.";
                $output["status"] = 2;
            }
            else
            {
                $output["error"] = "Error while sending the temporary password.";
                $output["status"] = 1;
            }
        }
        $this->set_response($output, REST_Controller::HTTP_OK);

    }

    // reset password - final step for forgotten password
    public function reset_password($code = NULL)
    {
        if (!$code)
        {
            show_404();
        }

        $user = $this->ion_auth->forgotten_password_check($code);

        if ($user)
        {
            // if the code is valid then display the password reset form

            $this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
            $this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

            if ($this->form_validation->run() == false)
            {
                // display the form

                // set the flash data error message if there is one
                $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

                $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
                $this->data['new_password'] = array(
                    'name' => 'new',
                    'id'   => 'new',
                    'type' => 'password',
                    'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
                );
                $this->data['new_password_confirm'] = array(
                    'name'    => 'new_confirm',
                    'id'      => 'new_confirm',
                    'type'    => 'password',
                    'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
                );
                $this->data['user_id'] = array(
                    'name'  => 'user_id',
                    'id'    => 'user_id',
                    'type'  => 'hidden',
                    'value' => $user->id,
                );
                $this->data['csrf'] = $this->_get_csrf_nonce();
                $this->data['code'] = $code;

                // render
                $this->_render_page('auth/reset_password', $this->data);
            }
            else
            {
                // do we have a valid request?
                if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
                {

                    // something fishy might be up
                    $this->ion_auth->clear_forgotten_password_code($code);

                    show_error($this->lang->line('error_csrf'));

                }
                else
                {
                    // finally change the password
                    $identity = $user->{$this->config->item('identity', 'ion_auth')};

                    $change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

                    if ($change)
                    {
                        // if the password was successfully changed
                        $this->session->set_flashdata('message', $this->ion_auth->messages());
                        redirect("auth/login", 'refresh');
                    }
                    else
                    {
                        $this->session->set_flashdata('message', $this->ion_auth->errors());
                        redirect('auth/reset_password/' . $code, 'refresh');
                    }
                }
            }
        }
        else
        {
            // if the code is invalid then send them back to the forgot password page
            $this->session->set_flashdata('message', $this->ion_auth->errors());
            redirect("auth/forgot_password", 'refresh');
        }
    }

    // logout user
    function logout_get(){
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $this->set_response($decodedToken, REST_Controller::HTTP_OK);
                $user_id = $this->input->get("user_id");
                $output=$this->Api_model->logout_user($user_id);   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message']
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "failure",
            'status_message' => "Unauthorised Access"
        ], REST_Controller::HTTP_UNAUTHORIZED);
    }

    /* ************** APIs after login starts here ****************** */
    
    /* Register User By Social Media */
    public function register_social_media_post(){
        $output=$this->Api_model->register_media_user($this->post());   
        $this->response($output, $output['response_code']); 
    }

    public function bodytype_get()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $this->set_response($decodedToken, REST_Controller::HTTP_OK);
                $output=$this->Api_model->bodytype();   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message'],
                    'result' => $output['result']
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);
    }

    public function personality_get()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $this->set_response($decodedToken, REST_Controller::HTTP_OK);
                $output=$this->Api_model->personality();   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message'],
                    'result' => $output['result']
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);
    }

    public function lifestyle_get()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $this->set_response($decodedToken, REST_Controller::HTTP_OK);
                $output=$this->Api_model->lifestyle();   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message'],
                    'result' => $output['result']
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);
    }

    public function ethinicity_get()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $this->set_response($decodedToken, REST_Controller::HTTP_OK);
                $output=$this->Api_model->ethinicity();   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message'],
                    'result' => $output['result']
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);
    }

    public function height_range_get()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $this->set_response($decodedToken, REST_Controller::HTTP_OK);
                $output=$this->Api_model->height_range();   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message'],
                    'result' => $output['result']
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);
    }

    public function attractiveness_get()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $this->set_response($decodedToken, REST_Controller::HTTP_OK);
                $output=$this->Api_model->attractiveness();   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message'],
                    'result' => $output['result']
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);
    }

    public function profession_get()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $this->set_response($decodedToken, REST_Controller::HTTP_OK);
                $output=$this->Api_model->profession();   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message'],
                    'result' => $output['result']
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);
    }

    public function update_profile_post()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $this->set_response($decodedToken, REST_Controller::HTTP_OK);
                $output=$this->Api_model->update_profile($decodedToken->id);   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message']
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);
    }

    public function update_attributes_post()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $this->set_response($decodedToken, REST_Controller::HTTP_OK);
                $output=$this->Api_model->update_user_attributes();   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message']
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);
    }

    public function update_match_attributes_post()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $this->set_response($decodedToken, REST_Controller::HTTP_OK);
                $output=$this->Api_model->update_users_match_attributes();   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message']
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);
    }

    public function add_new_profile_post()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $this->set_response($decodedToken, REST_Controller::HTTP_OK);
                $output=$this->Api_model->add_new_profile();   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message']
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);
    }

    public function dating_profiles_get()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $this->set_response($decodedToken, REST_Controller::HTTP_OK);
                $output=$this->Api_model->get_dating_profiles();   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message'],
                    'result'=> $output['result']
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);
    }

    public function update_other_profile_post()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $this->set_response($decodedToken, REST_Controller::HTTP_OK);
                $output=$this->Api_model->update_other_profile();   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message']
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);
    }

    public function profile_details_get()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $this->set_response($decodedToken, REST_Controller::HTTP_OK);
                $output=$this->Api_model->user_profile_details();   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message'],
                    'result' => $output['result']
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);
    }


    /*
        Fetch all slider records
    */
    public function slider_get()
    {      
        $output=$this->Api_model->sliders();   
        $this->response([
            'status'=> $output['status'],
            'status_message'=> $output['status_message'],
            'result' => $output['result']
        ], $output['response_code']);       
    }


    /*
        Fetch all attributes
    */
    public function get_all_attributes_get()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $output=$this->Api_model->get_all_attributes($decodedToken->id);   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message'],
                    'result' => $output['result']
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);

    }

    /*
        Update all attributes
    */
    public function update_user_attributes_post()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $output=$this->Api_model->update_all_attributes($decodedToken->id, $this->post());   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message'],
                    'result' => $output['result']
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);

    }


    /*
        Get User Information
    */
    public function get_user_info_get()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $output=$this->Api_model->get_user_info($decodedToken->id);   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message'],
                    'result' => $output['result']
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);

    }


    /*
        Fetch all attributes
    */
    public function get_all_user_attributes_get()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $output=$this->Api_model->get_all_user_attributes($decodedToken->id);   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message'],
                    'result' => $output['result']
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);

    }


    /*
        Update about user
    */
    public function update_about_user_post()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $output=$this->Api_model->update_about_user($decodedToken->id, $this->post());   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message'],
                    'result' => $output['result']
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);
    }


    /*
        Update ideal match
    */
    public function update_ideal_match_post()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $output=$this->Api_model->update_ideal_match($decodedToken->id, $this->post());   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message'],
                    'result' => $output['result']
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);
    }


    /*
        Fetch Apps List
    */
    public function app_list_get()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $output=$this->Api_model->get_app_list($decodedToken->id);   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message'],
                    'result' => $output['result']
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);
    }


    /*
        Add New Profile to Dating List
    */
    public function add_new_match_post()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $this->set_response($decodedToken, REST_Controller::HTTP_OK);
                $output=$this->Api_model->add_new_match($decodedToken->id, $this->post());   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message'],
                    'result' => $output["result"]
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);
    }


    /*
        Update dating profile
    */
    public function update_match_post($id = null)
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $this->set_response($decodedToken, REST_Controller::HTTP_OK);
                $output=$this->Api_model->update_match($decodedToken->id, $id, $this->post());   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message'],
                    'result' => $output["result"]
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);
    }


    /*
        Get matches
    */
    public function get_matches_get()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $this->set_response($decodedToken, REST_Controller::HTTP_OK);
                $output=$this->Api_model->get_matches($decodedToken->id);   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message'],
                    'result' => $output["result"]
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);
    }


    /*
        Archive a match
    */
    public function archive_match_post($id = null)
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $this->set_response($decodedToken, REST_Controller::HTTP_OK);
                $output=$this->Api_model->archive_match($decodedToken->id, $id);   
                $this->response([
                    'status'=> $output['status'],
                    'status_message'=> $output['status_message'],
                    'result' => $output["result"]
                ], $output['response_code']);
            }
        }
        $this->response([
            'status'=> "error",
            'status_message'=> 'Unauthorised'
        ], REST_Controller::HTTP_UNAUTHORIZED);
    }


}