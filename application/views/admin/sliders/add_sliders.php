<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Sliders
        <small>Add Slider</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('Home');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url('Sliders');?>">Sliders</a></li>
        <li class="active">Add Slider</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <!-- <form role="form"> -->
        <?php echo form_open_multipart(''); ?>
            <?php 
                if(isset($error)){
            ?>
            <div class="alert alert-danger">
              <?php echo $error; ?>
            </div>
            <?php
                } 
            ?>
            <div class="box-body">
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" placeholder="Enter Title" name="db_title" value="<?php echo set_value('db_title', @$t->title);?>">
                    <?php echo form_error('db_title'); ?>
                </div>                
                <div class="form-group">
                    <label for="description">Description</label>
                    <input type="text" class="form-control" id="description" placeholder="Enter Email" name="db_description" value="<?php echo set_value('db_description', @$t->description);?>">
                    <?php echo form_error('db_description'); ?>
                </div>
                <div class="form-group">
                    <label for="images">Image</label>
                    <input type="file" class="form-control" id="images" name="db_image">
                    <?php echo form_error('db_image'); ?>
                </div>
                <div class="form-group">
                    <label for="sort_order">Sort Order</label>
                    <input type="number" id="sort_order" class="form-control" placeholder="Enter Sort Order" name="db_sort_order" value="<?php echo set_value('db_sort_order', @$t->sort_order);?>">
                    <?php echo form_error('db_sort_order'); ?>
                </div>
                <div class="form-group">
                    <label for="status">Status</label>
                    <select name="status" id="status" class="form-control">
                        <option value="0">Inactive</option>
                        <option value="1" selected>Active</option>
                    </select>
                    <?php echo form_error('db_status'); ?>
                </div>                 
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        <?php echo form_close(); ?>
    </div>
    <!-- /.box -->
</div>
<!--/.col (left) -->
<!-- right column -->

<!--/.col (right) -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->