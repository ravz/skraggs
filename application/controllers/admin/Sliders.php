<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sliders extends CI_Controller {

    var $data;

    function __construct() {
        parent::__construct();

        $this->load->model("slider_model");

        if (!$this->ion_auth->logged_in() OR !$this->ion_auth->is_admin()) {
            redirect('admin/login');
        }

    }

	public function index() {
        $this->data['sliders'] = $this->slider_model->list_sliders();
        $this->template->load('admin/index', 'admin/sliders/list_sliders', $this->data);
    }

    public function add() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('db_title', 'Title', 'required|max_length[100]');
        $this->form_validation->set_rules('db_description', 'Description', 'required|max_length[255]');
        $this->form_validation->set_rules('db_sort_order', 'Sort Order', 'required|max_length[100]');

        if ($this->form_validation->run() === FALSE) {

            $this->template->load('admin/index', 'admin/sliders/add_sliders', $this->data);
        }
        else {
            
            $file = "";
            $config['upload_path']          = './uploads/sliders/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 2000;
            $config['max_width']            = 1024;
            $config['max_height']           = 768;
            $config['encrypt_name']         = true;
            

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('db_image'))
            {
                    $error = array('error' => $this->upload->display_errors());
                    $this->template->load('admin/index', 'admin/sliders/add_sliders', $error);
                    return;
            }
            else
            {
                    $data = $this->upload->data();
                    $file = $data["file_name"];
            }

            if ($this->slider_model->add($this->input->post(), $file)) {
                redirect('admin/sliders');
            }
        }
    }

    public function edit($id = NULL) {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('db_title', 'Title', 'required|max_length[100]');
        $this->form_validation->set_rules('db_description', 'Description', 'required|max_length[255]');
        $this->form_validation->set_rules('db_sort_order', 'Sort Order', 'required|max_length[100]');

        $this->data["t"] = $this->slider_model->get_slider_info($id);
        if ($this->form_validation->run() === FALSE) {
            $this->template->load('admin/index', 'admin/sliders/edit_sliders', $this->data);
        }
        else {
            
            $file = $this->input->post("edit_image");

            if(isset($_FILES["db_image"]["name"]) && $_FILES["db_image"]["name"] != ""){
                $config['upload_path']          = './uploads/sliders/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 2000;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;
                $config['encrypt_name']         = true;
                

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('db_image'))
                {
                        $this->data["error"] = $this->upload->display_errors();
                        $this->template->load('admin/index', 'admin/sliders/edit_sliders', $this->data);
                        return;
                }
                else
                {
                        $data = $this->upload->data();
                        $file = $data["file_name"];
                }
            }

            if ($this->slider_model->edit($this->input->post(), $file)) {
                redirect('admin/sliders');
            }
        }
    }

    public function change($id) {

        $this->load->library('form_validation');
        if ($id !== '') {
            $this->form_validation->set_rules('db_password', 'Password', 'required|max_length[255]');

            if ($this->form_validation->run() === FALSE) {
                $this->template->load('admin/index', 'admin/users/change_password_user');
            }
            else {
                $id = base64_decode($this->input->post('aWQ='));
                $pass = $this->input->post('db_password');

                if ($this->ion_auth->update($id, array('password' => $pass))) {
                    redirect('admin/users');
                }
            }
        }
    }

    public function delete($id) {

        $id = (int)$id;

        if ($id !== '' AND $id > 0) {
            if ($this->db->query("DELETE FROM `sliders` WHERE `id` = ".$id)) {
                redirect('admin/sliders');
            }
        }
    }
}
