$(document).ready(function(e){

	/*Main menu - Location dropdown*/
	$(function(){
    	$(".dropdown").hover(
    		function(){
                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
              	$(this).toggleClass('open');
               // $('b', this).toggleClass("caret caret-up");                
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                $(this).toggleClass('open');
              //  $('b', this).toggleClass("caret caret-up");                
        	});
    });
    /*Search Event*/
    $('.submit').click(function(event){ 		
		if($('.field').css('width') < "7"){
			$('.field').addClass('width');
			return false;    	   
		}else{
			return true;
		}
	});
	
	$(document).click(function(e) {
    if(( e.target.id != 'search-label') && ( e.target.id != 'btn_get')) { 
		 $('.field').removeClass('width');
		} 	
	});

	// Cart - Remove product
	$('a.cart_cancel[href="#"]').click(function() {
		console.log($(this).parent().parent().attr('class'));
		$(this).parent().parent().remove();
		//location.reload();
	});

	$('a.cart_cancel[href="#"]').click(function() {
		console.log($(this).parent().parent().attr('class'));
		$(this).parent().parent().remove();
		//location.reload();
	});
	
    /* Login page */
	$(function() {

	    $('#login-form-link').click(function(e) {
			$("#login-form").delay(100).fadeIn(100);
	 		$("#register-form,#new-vendor").fadeOut(100);
			$('.panel-tabs').removeClass('active');
			$(this).addClass('active');
			e.preventDefault();
		});
		
		$('#new-vendor-link').click(function(e) {
			$("#new-vendor").delay(100).fadeIn(100);
	 		$("#login-form").fadeOut(100);
	 		$("#register-form,#login-form").fadeOut(100);
			$('.panel-tabs').removeClass('active');
			$(this).addClass('active');
			e.preventDefault();
		});

	});

	/* Quicklinks 
	 if (windowsize >= 768) {
		$( ".quicklinks li" ).each(function() {
		   var imgHeight=$(this).find('img').outerHeight();
		   $(this).css('height', imgHeight);
		});
	}*/

	// Sign In Registration
	$('.sign_continue').click(function(){
		$('.signin-registration').fadeIn(300);
		$('.signin-sec').fadeOut(300);
	})
	$('.vendor_coninue').click(function(){
		$('.vendor_registration').fadeIn(300);
		$('.vendor_signin').fadeOut(300);
	})
	
	// Shop Page Icon Fixed
	var findShopIconDiv=$('.checkout_icon ,.similar_exp');	
	var findCheckOutDiv=$('.checkout_icons');
	var findCheckOutSidebarDiv=$('#checkout_form');
	var findDiv=$('.similar_exp');

	function checkIcon(){		
		if(findCheckOutDiv.length!=0){ 
			if($(window).scrollTop() > 230){		
			   //If the current scroll position is more than 100, add class
			   	$( "#space_check" ).addClass("inner_top_align ");
				$( "#order_accept" ).addClass("inner_top_order ");
				$( "#checkout_icon" ).addClass("fixed-pos");
				//,#order_accept
			} else{
				//Else remove it
				$( "#space_check" ).removeClass("inner_top_align");
				$( "#order_accept" ).removeClass("inner_top_order");
				$( "#checkout_icon" ).removeClass("fixed-pos");
				
			}
		}
	}

	var checkContentHeight=$('.checkout-fix').outerHeight();
	
	

	//var $h1 = $("footer").offset().top-checkContentHeight;
	function checkOutSidebar(){		
		if($(".my_order").outerHeight()<558){
			var myOrder=558;
		}else{
			var myOrder=$(".my_order").outerHeight();
		}
		winheight=$("footer").offset().top - myOrder;
		var order_div = $(".my_order").innerHeight() + 100;
		if(winIsSmall){
			if(findCheckOutDiv.length!=0){ 
				   if(($(window).scrollTop() > 250) && ($(window).scrollTop() < order_div) ){	

				   $( "#space_check" ).addClass("checkout-fix");
				   $( "#space_check" ).removeClass("checkout-rel");
				   $( ".checkout-fix .tab-content" ).css('margin-top',60+"px");

				// console.log(winheight);
					
				}else if($(window).scrollTop() > order_div){ 
					 $( "#space_check" ).removeClass("checkout-fix");
					 $( "#space_check" ).addClass("checkout-rel");
					 $( ".checkout-rel .tab-content" ).css('margin-top',60+"px");
					// console.log(winheight);
				
				}else{
					 $( "#space_check" ).removeClass("checkout-fix");
					 $( "#space_check" ).removeClass("checkout-rel");;
					 $( ".checkout-rel .tab-content" ).css('margin-top',60+"px");
					//  console.log(winheight);
				}
			}
		}
		else{
				$( "#space_check" ).removeClass("checkout-fix");
				$( "#space_check" ).removeClass("checkout-rel");
		}
	}
    
	
	
	function shopIcon(){	
     if(winIsSmall){
		if(findShopIconDiv.length!=0){ 
			if($(window).scrollTop() > $('.checkout_icon ,.similar_exp').offset().top) {		
			   //If the current scroll position is more than 100, add class
				$( "#checkout_icon" ).addClass("fixed-pos");
				$( "#space" ).addClass("inner_top_align");
			} else {
				//Else remove it
				$( "#checkout_icon" ).removeClass("fixed-pos");
		$( "#space" ).removeClass("inner_top_align");
			}
		}
	 }
	 else{
		 $( "#checkout_icon" ).removeClass("fixed-pos");
		 $( "#space" ).removeClass("inner_top_align");
	 }
	}
	
	// Single product Page Menu Fixed	
	
	function singlePageMenu(){
		
		var scroll_top = $(window).scrollTop(); // our current vertical position from the top
		var headerHeight=$('#mainBanner').outerHeight() + 75;
		var checkheight = $(".single_pro_desc").outerHeight() - 30;
		var twoHeight =  headerHeight + checkheight;

		if((scroll_top > headerHeight) && (scroll_top < twoHeight)){	
			$( ".custom_tab" ).addClass("custom_pills-fixed");
		}
		else{
			$( ".custom_tab" ).removeClass("custom_pills-fixed");
		}
	}
	
	// Single product Page Sidebar Fixed
	
	var winIsSmall;
	
	function WinSize(){
		winIsSmall= $(window).width() >= 767; // BOOLEAN
    }
    $(window).on("load resize", WinSize);
	
	
	function singleSidebar() { //OnScroll, invoke
		  
		if(winIsSmall){

			if(findDiv.length!=0){ 
			  
			    if($(window).scrollTop() > $('.similar_exp').offset().top - 500) {
					  
					var txt = $(".check_height").height();
					$( ".product_fixed" ).parent().css({ height: txt+"px" });
				    //If the current scroll position is more than 100, add class
			        $( ".product_fixed" ).addClass("product_fixed_remove");
				 } 
				 else{ 
					$( ".product_fixed" ).removeClass("product_fixed_remove");   
				 }
			}
	    }
	    else{ 
		  //Else remove it
	   	  $( ".product_fixed" ).removeClass("product_fixed_remove");   
	    }
		 
	}
		
	/* Account Sidebar Menu 
	
	function accountSidebarMenu(){
    var account_div = $(".account_sec").innerHeight() - 350;		
			if($(window).scrollTop() > 150 && $(window).scrollTop() < account_div){		
			   //If the current scroll position is more than 100, add class
				$( "#sidebar-nav" ).addClass("fixed-pos");	
				
				//,#order_accept
			}
			else{
				//Else remove it
				$( "#sidebar-nav" ).removeClass("fixed-pos");				
			}
	} */
	
	$(window).scroll(function() { //OnScroll, invoke
		shopIcon();
		singlePageMenu();
		singleSidebar();
		checkIcon();
		checkOutSidebar();
	});

	// Account Page 
	$(window).load(function() {
		if(winIsSmall){
			var accMenu=$('.con_white').outerHeight();
			var accMenu=$('.con_white').outerHeight();
			$('.account_menu,.con_grey').css('height',accMenu);
		}
	});

});

/* $('a.scroll_icon').click(function(e){
	e.preventDefault();
	$("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top - 150}, {duration: 1000, easing: "swing" });
}); */

$('a.scroll_icon[href^="#"]').click(function() {
  if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    if (target.length) {
      $('html, body').animate({
        scrollTop: target.offset().top - 130 +'px'
      }, 1000);
      return false;
    }
  }
});

$("input[type='image']").click(function() {
    $("input[id='my_file']").click();
});

/*$(document).ready(function(){
	$('a.scroll_icon[href^="#"]').on('click',function (e) {
	    e.preventDefault();

	    var target = this.hash;
	    var $target = $(target);

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top - 130
	    }, 900, 'swing', function () {
	        window.location.hash = target;
	    });
	});
}); */

$('a.list_type').click(function(e){
	$('a.list_type').parent('li').removeClass("active");
	$(this).parent('li').addClass("active");
});

$('a.list_type').click(function(e){
	e.preventDefault();
	$("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top - 75 +'px' }, {duration: 500, easing: "swing" });
});

$('.sidebar-nav .nav-list a').click(function(e){
	//e.preventDefault();
	$("html, body").animate({ scrollTop: $($(this).attr("data-href")).offset().top }, {duration: 500, easing: "swing" });
});

$('#final_set').click(function(e){
	$("#space_check").css({"display":"none"});
	$("#order_accept").css({"display":"block"});
	$('.wizard .nav-tabs > li:not(:last)').addClass('disabled');
});


$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {    
    var target = $(this).attr('href');

    $(target).css('left','-'+$(window).width()+'px');   
    var left = $(target).offset().left;
    $(target).css({left:left}).animate({"left":"0px"}, 10);
});

/* $(document).on("scroll", onScroll);

function onScroll(event){
    var scrollPos = $(document).scrollTop();
    $('a.list_type').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
			//$('a.list_type').parent('li').removeClass("active");
            currLink.parent('li').addClass("active");
        }
        else{
            currLink.parent('li').removeClass("active");
        }
    });
} */

// Menu Overlay 

var root = document.getElementsByTagName( 'html' )[0]; // '0' to assign the first (and only `HTML` tag)

function overlay_effect() {
	root.setAttribute( 'class', 'remodal-is-locked' );
	$("body").css({"padding-right":"17px"});
}
function overlay_removed() {
		document.getElementById("overlay_effect").style.display = "none";
		root.setAttribute( 'class', '' );
		$("body").css({"padding-right":"0px"});
}

$('#overlay_effect').click(function(){
	    overlay_removed();
	    document.getElementById("mainSidenav").style.marginLeft = "-500px";
		document.getElementById("mainlogin").style.marginRight = "-500px";
	    document.getElementById("maincart").style.marginRight = "-500px";
});

// SLIDE THE NAVBAR DOWN / UP
function openNav(){ 
    overlay_effect();
    document.getElementById("overlay_effect").style.display = "block";
	document.getElementById("mainSidenav").style.marginLeft = "0";
	document.getElementById("mainlogin").style.marginRight = "-500px";
	document.getElementById("maincart").style.marginRight = "-500px";
}

function closeNav(){
	overlay_removed();
	document.getElementById("overlay_effect").style.display = "none";
    document.getElementById("mainSidenav").style.marginLeft = "-500px";
}
function openloginNav(){
	overlay_effect();
	document.getElementById("overlay_effect").style.display = "block";
	document.getElementById("mainlogin").style.marginRight = "0";
	document.getElementById("mainSidenav").style.marginLeft = "-500px";
}

function closeologinNav(){
	overlay_removed();
	document.getElementById("overlay_effect").style.display = "none";
    document.getElementById("mainlogin").style.marginRight = "-500px";
}

function opencart(){
	overlay_effect();
	document.getElementById("overlay_effect").style.display = "block";	
	document.getElementById("maincart").style.marginRight = "0";
	document.getElementById("mainSidenav").style.marginLeft = "-500px";
}
function closecart(){
	overlay_removed();
	document.getElementById("overlay_effect").style.display = "none";
    document.getElementById("maincart").style.marginRight = "-500px";
}
function modalTextChange(){ 
    document.getElementById("accept").style.display = "none";
	document.getElementById("denied").style.display = "block";
}

/* Checkout Tabs */

$(document).ready(function () {
    //Initialize tooltips
   // $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}

$(function() {
	  $('.star_rating').barrating({
		theme: 'fontawesome-stars'
	  });
	  
   'use strict';
	var countries = {
		"CA": "California",
		"FL": "Florida",
		"TX": "Texas",
		"HI": "Hawaii",
		"AK": "Alaska",
		"MA": "Massachusetts",
		"AL": "Virinia",
		"NJ": "New Jersey",
		"AL": "Alabama",
		"PA": "Pennsylvania",
		"GA": "Georgia",
		"MN": "Minnesota",
		"NC": "North Carolina",
		"AR": "Arizona",
		"IL": "Illinois",
		"CO": "Colorado",
		"OH": "Ohio",
		"TN": "Tennessee",
		"MI": "Michigan",
		"WA": "Washington",
		"OR": "Oregon",
		"MO": "Missouri",
		"NY": "New York",
		"OK": "Oklahoma",
		"MD": "Maryland",
		"CT": "Connecticut",
		"LA": "Louisiana",
		"SC": "South Carolina",
		"UT": "Utah",
		"MS": "Mississippi",
		"IN": "Indiana",
		"KY": "Kentucky",
		"NE": "Nebraska",
		"NV": "Nevada",
		"NM": "New Mexico",
		"ME": "Maine",
		"AR": "Arkansas",
		"RI": "Rhode Island",
		"KS": "Kansas",
		"NH": "New Hampshire",
		"IA": "Iowa",
		"WY": "Wyoming",
		"ID": "Idaho",
		"DE": "Delaware",
		"VT": "Vermont",
		"WV": "West Virgina",
		"ND": "North Dakota",
		"SD": "South Dakota",
		"MT": "Montana",
		"VA": "Virginia",
		"WV": "West Virginia",
		"WI": "Wisconsin"
		}
	var countriesArray = $.map(countries, function (value, key) { return { value: value, data: key }; });

	// Initialize ajax autocomplete:
	$('#location_all').autocomplete({
		// serviceUrl: '/autosuggest/service/url',
		lookup: countriesArray,
		lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
			var re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
			return re.test(suggestion.value);
		},
		onSelect: function(suggestion) {
			$(".region_name h3").remove();
			//$('.region_name').html('You selected: ' + suggestion.value + ', ' + suggestion.data);
			$(".region_name").append('<h3>' + suggestion.value + ',' + suggestion.data + '</h3>');
			$(".country_code").empty();
			$(".country_code").append('' + suggestion.data + '');
			localStorage.setItem("server", suggestion.data);
		},
		onHint: function (hint) {
			$('#autocomplete-x').val(hint);
		},
		showNoSuggestionNotice: true,
		noSuggestionNotice: 'Sorry, No results',
		groupBy: 'category'
	});

	$(document).ready(function() {
		  var storedValue = localStorage.getItem("server");
		  $(".region_name h3").remove();
		  $(".country_code").empty();
		  $(".country_code").append('' + storedValue + '');
	});
	
	$('#location_btn').click(function(e){
		$.getJSON('http://freegeoip.net/json/', function (location) {
		$(".country_code").empty();
		$(".country_code").append('' + location.region_code + '');
		$(".region_name h3").remove();
		$(".region_name").append('<h3>' + location.region_name +','+ location.region_code + '</h3>');
		$('#location_all').val('' + location.region_name + '');
		localStorage.setItem("server", location.region_code);
		});  
	});
});